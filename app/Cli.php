<?php

namespace App;

/**
 * Class Cli
 * @author Andrii Tomchyshyn
 * @package App
 */
class Cli
{

    /**
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        try {
            $module = $this->detectModule();
            $action = $this->detectAction($module);
            $params = $this->detectParams($module, $action);
            $result = call_user_func_array([$module, $action], $params);
            $this->print($result);
        } catch (\Exception $exception) {
            $this->print("\033[0;31m" . $exception->getMessage() . "\033[0m" . PHP_EOL);
        }
    }

    /**
     * Try to detect Action from passed arguments via CLI
     *
     * @return \AbstractModule
     * @throws \Exception
     */
    protected function detectModule() : \AbstractModule
    {
        $options = getopt('m:');
        if (empty($options['m'])) {
            $scripts = glob(__DIR__ . '/../library/*.php');
            $modules = [];
            foreach ($scripts as $scriptPath) {
                $module = str_replace('.php', '', basename($scriptPath));
                if (is_subclass_of($module, \AbstractModule::class)) {
                    $modules[] = strtolower(preg_replace("/(.)([A-Z])/", '$1-$2', $module));
                }
            }

            throw new \Exception(
                sprintf(
                    "Please specify one of modules to run!\n> php %s -m (%s) ",
                    $_SERVER['SCRIPT_FILENAME'] ?? '*script.php*',
                    implode('|', $modules)
                )
            );
        }

        $module = str_replace(' ', '', ucwords(str_replace('-', ' ', explode('::', $options['m'])[0])));
        if (false === class_exists($module)) {
            throw new \Exception("Module `{$module}` does not exists!");
        }

        if (false === is_subclass_of($module, \AbstractModule::class)) {
            throw new \Exception("Module `{$module}` does not extend \\AbstractModule!");
        }

        return new $module;
    }

    /**
     * Try to detect Action
     *
     * @param \AbstractModule $module module
     *
     * @return string
     * @throws \Exception
     */
    protected function detectAction(\AbstractModule $module) : string
    {
        // Try to detect Action from passed arguments
        $options = getopt('m:');
        $action = explode('::', $options['m'])[1] ?? null;

        if (empty($action)) {
            // Detect available Actions from Module.
            // In case available is only the one, use it.
            // In other cases generate error.
            $action = null;
            $publicMethods = get_class_methods($module);
            switch (count($publicMethods) <=> 1) {
                case 0:
                    $action = reset($publicMethods);
                    break;
                case 1:
                    throw new \Exception(
                        sprintf(
                            "Module `%1\$s` has few public methods. Please specify the one you need!\n"
                            . '> php %2$s -m %1$s::(%3$s)',
                            get_class($module),
                            $_SERVER['SCRIPT_FILENAME'] ?? '*script.php*',
                            implode('|', $publicMethods)
                        )
                    );
                case -1:
                    throw new \Exception(
                        sprintf('Error! Module `%s` has no public methods to call!', get_class($module))
                    );
            }
        }

        if (empty($action) || false === is_callable([$module, $action])) {
            throw new \Exception(
                sprintf(
                    "Method `%s::%s(...)` can not be called. Method not exists or it is not callable.\n"
                    . 'Please choose one of (::%s)',
                    get_class($module),
                    $action,
                    implode('|::', get_class_methods($module))
                )
            );
        }

        return $action;
    }

    /**
     * Try to detect Action Parameters from passed arguments via CLI
     *
     * @param \AbstractModule $module module
     * @param string          $action action
     *
     * @return array
     * @throws \Exception
     */
    protected function detectParams(\AbstractModule $module, string $action) : array
    {
        global $argv; // Needed for correct detecting of empty arguments e.g. `--text=""`
        $specifiedParameters = [];
        $missedRequiredParams = [];
        $reflection = new \ReflectionMethod($module, $action);
        $parameters = $reflection->getParameters();
        foreach ($parameters as &$parameter) {
            $params = getopt('m:', [$parameter->getName() . ':']);
            if (isset($params[$parameter->getName()])) {
                $specifiedParameters[] = $params[$parameter->getName()];
            } elseif (false === empty($argv) && in_array("--{$parameter->getName()}=", $argv)) {
                $specifiedParameters[] = '';
            } elseif (false === $parameter->isOptional()) {
                $missedRequiredParams[] = $parameter->getName();
            } else {
                $specifiedParameters[] = $parameter->getDefaultValue();
            }
        }

        if ($missedRequiredParams) {
            throw new \Exception(
                sprintf(
                    "Some required parameters are missing! Please specify them.\n > . . . --%s=...",
                    implode('=... --', $missedRequiredParams)
                )
            );
        }

        return $specifiedParameters;
    }

    /**
     * @param mixed $result result
     *
     * @return void
     */
    protected function print($result) : void
    {
        if (is_array($result) && is_numeric(key($result))) {
            echo implode("\n" , $result);
        } else {
            print_r($result);
        }

        echo PHP_EOL;
    }

}
