<?php

require_once __DIR__ . "/vendor/autoload.php";

$configurations = [
    \MovingParticles::class . '::animate' => [
        '50100x50' => dataGenMovingParticles(50100, [1,50]),
        '1x500100' => dataGenMovingParticles(1, 500100, 25001),
    ],
    \PangramInspector::class . '::getMissingLetters' => [
        '50100x50'          => dataGenPangramInspector(50100, [0,50]),
        '1x20100100'        => dataGenPangramInspector(1, 20100100),
        '1x20100100-no-CDE' => dataGenPangramInspector(1, 20100100, 'cde'),
    ]
];

foreach ($configurations as $configKey => $generators) {
    list($moduleName, $moduleAction) = explode('::', $configKey);
    $module = new $moduleName();
    foreach ($generators as $index => $dataGenerator) {
        echo "\nStarting Performance Test: `{$moduleName}::{$moduleAction}` $index \n";

        $processingTime = 0;
        $generatingTime = 0;
        $outputtingTime = 0;

        $outputFile = "var/output-{$moduleName}-{$moduleAction}-{$index}.txt";
        $outputFileHandler = fopen($outputFile, 'w');

        $timePoint = microtime(true);
        foreach ($dataGenerator as $arguments) {
            $generatingTime += microtime(true) - $timePoint;

            $timePoint = microtime(true);
            $result = call_user_func_array([$module, $moduleAction], (array)$arguments);
            $processingTime += microtime(true) - $timePoint;

            $timePoint = microtime(true);
            $argumentsStr = is_array($arguments) ? implode(',', $arguments) : $arguments;
            $resultStr    = is_array($result) ? implode("\n", $result) : $result;
            fputs($outputFileHandler, sprintf("%s\n%s\n\n", $argumentsStr, $resultStr));
            $outputtingTime += microtime(true) - $timePoint;

            $timePoint = microtime(true);
        }

        fclose($outputFileHandler);

        echo "Generating Time: $generatingTime\n";
        echo "Processing Time: $processingTime\n";
        echo "Outputting Time: $outputtingTime\n";
        echo "Output File: $outputFile\n___\n";
    }
}

/**********************************
 * Data Generators
 **********************************/

/**
 * @param int       $iterations iterations
 * @param int|array $size       size
 * @param int       $speed      speed
 *
 * @return Generator
 */
function dataGenMovingParticles(int $iterations, $size, int $speed = null)
{
    $charList = ['R', 'L', '.'];
    for ($i = 0; $i < $iterations; ++$i) {
        $chamberSize = is_array($size) ? rand($size[0], $size[1]) : $size;
        $chamber = '';
        for ($j = 0; $j <= $chamberSize; ++$j) {
            $chamber .= $charList[rand(0, 2)];
        }

        yield [$speed ?? rand(1, is_array($size) ? $size[1] : $size), $chamber];
    }
}

/**
 * @param int       $iterations  iterations
 * @param int|array $size        size
 * @param string    $bannedChars banned characters
 *
 * @return Generator
 */
function dataGenPangramInspector(int $iterations, $size, string $bannedChars = '')
{
    for ($i = 0; $i < $iterations; ++$i) {
        $textSize = is_array($size) ? rand($size[0], $size[1]) : $size;
        $text = '';
        for ($j = 0; $j <= $textSize; ++$j) {
            do {
                $char = chr(rand(0, 255));
            } while (false !== stripos($bannedChars, $char));
            $text .= $char;
        }

        yield $text;
    }
}
