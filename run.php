<?php

ini_set('memory_limit', '512M');
require_once __DIR__ . "/vendor/autoload.php";

(new App\Cli)->run();
