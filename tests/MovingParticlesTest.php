<?php

/**
 * Class MovingParticlesTest
 * @author Andrii Tomchyshyn
 */
class MovingParticlesTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var MovingParticles
     */
    protected static $movingParticlesObj;

    /**
     * @return void
     */
    public static function setUpBeforeClass() : void
    {
        self::$movingParticlesObj = new MovingParticles();
    }

    /**
     * @return void
     */
    public static function tearDownAfterClass() : void
    {
        self::$movingParticlesObj = null;
    }

    /**
     * @param string $init init
     *
     * @return void
     * @throws Exception
     *
     * @testWith [".."]
     *           ["..RR."]
     *           ["L."]
     *           [".LRL."]
     */
    public function testValidateInit($init) : void
    {
        $this->assertEquals($init, self::$movingParticlesObj->validateInit($init));
    }

    /**
     * @param string $init init
     *
     * @return void
     * @throws Exception
     *
     * @testWith [""]
     *           ["..rR."]
     *           [" L "]
     *           ["1010"]
     */
    public function testValidateInitFailureFormat($init) : void
    {
        $this->expectExceptionMessageRegExp("/^Init parameter defines.*/");
        self::$movingParticlesObj->validateInit($init);
    }

    /**
     * @param string $init init
     *
     * @return void
     * @throws Exception
     *
     * @testWith [1]
     *           ["5"]
     *           [1000]
     *           [3.2]
     *           [4.8]
     */
    public function testValidateSpeed($init) : void
    {
        $this->assertEquals(floor($init), self::$movingParticlesObj->validateSpeed($init));
    }

    /**
     * @param string $init init
     *
     * @return void
     * @throws Exception
     *
     * @testWith [0]
     *           [-5]
     *           [0.5]
     */
    public function testValidateSpeedFailureFormat($init) : void
    {
        $this->expectExceptionMessageRegExp("/^Speed parameter must.*/");
        self::$movingParticlesObj->validateSpeed($init);
    }

    /**
     * @param int    $speed speed
     * @param string $init  init
     * @param string $trace trace
     *
     * @return void
     * @throws Exception
     *
     * @depends testValidateInit
     * @depends testValidateSpeed
     *
     * @dataProvider dataProviderAnimate
     */
    public function testAnimate($speed, $init, $trace) : void
    {
        $this->assertEquals($trace, self::$movingParticlesObj->animate($speed, $init));
    }

    /**
     * @return array
     */
    public function dataProviderAnimate() : array
    {
        return [
            [
                2,
                '..R....',
                [
                    '..X....',
                    '....X..',
                    '......X',
                    '.......',
                ]
            ],
            [
                3,
                'RR..LRL',
                [
                    'XX..XXX',
                    '.X.XX..',
                    'X.....X',
                    '.......',
                ]
            ],
            [
                2,
                'LRLR.LRLR',
                [
                    'XXXX.XXXX',
                    'X..X.X..X',
                    '.X.X.X.X.',
                    '.X.....X.',
                    '.........',
                ]
            ],
            [
                10,
                'RLRLRLRLRL',
                [
                    'XXXXXXXXXX',
                    '..........',
                ]
            ],
            [
                1,
                '...',
                ['...']
            ],
            [
                1,
                'LRRL.LR.LRR.R.LRRL.',
                [
                    'XXXX.XX.XXX.X.XXXX.',
                    '..XXX..X..XX.X..XX.',
                    '.X.XX.X.X..XX.XX.XX',
                    'X.X.XX...X.XXXXX..X',
                    '.X..XXX...X..XX.X..',
                    'X..X..XX.X.XX.XX.X.',
                    '..X....XX..XX..XX.X',
                    '.X.....XXXX..X..XX.',
                    'X.....X..XX...X..XX',
                    '.....X..X.XX...X..X',
                    '....X..X...XX...X..',
                    '...X..X.....XX...X.',
                    '..X..X.......XX...X',
                    '.X..X.........XX...',
                    'X..X...........XX..',
                    '..X.............XX.',
                    '.X...............XX',
                    'X.................X',
                    '...................',
                ]
            ],
        ];
    }

}
