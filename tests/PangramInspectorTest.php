<?php

/**
 * Class PangramInspectorTest
 * @author Andrii Tomchyshyn
 */
class PangramInspectorTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @param string $text     text
     * @param string $expected expected
     *
     * @return void
     *
     * @testWith ["A quick brown fox jumps over the lazy dog", ""]
     *           ["A slow yellow fox crawls under the proactive dog", "bjkmqz"]
     *           ["Lions, and tigers, and bears, oh my!", "cfjkpquvwxz"]
     *           ["", "abcdefghijklmnopqrstuvwxyz"]
     */
    public function testGetMissingLetters(string $text, string $expected) : void
    {
        $this->assertEquals($expected, (new PangramInspector)->getMissingLetters($text));
    }

}
