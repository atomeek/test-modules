##Preparing Virtual Machine:
* Install VirtualBox
* Install Vagrant
* Start and configure your virtual machine. Run `vagrant up` and `vagrant ssh`

##Working with a project:
* `# php run.php` - Main APP interface. Provides you all necessary information about command execution
* `# php test-performance.php ` - Test APP performance
* `# sh ./run-tests.sh` - Run all PHPUnit tests
