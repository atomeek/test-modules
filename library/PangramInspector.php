<?php

/**
 * Class PangramInspector
 * @author Andrii Tomchyshyn
 */
class PangramInspector extends AbstractModule
{

    /**
     * @var string
     */
    private $alphabet = 'abcdefghijklmnopqrstuvwxyz';

    /**
     * Returns letters from the alphabet that are not contained in the specified text
     *
     * @param string $text text
     *
     * @return string
     */
    public function getMissingLetters(string $text) : string
    {
        $alphabet = $this->alphabet . strtoupper($this->alphabet);
        do {
            $text = strpbrk($text, $alphabet);
            $alphabet = str_ireplace(substr($text, 0, 1), '', $alphabet);
        } while (false == empty($text) && false == empty($alphabet));

        return preg_replace("/[A-Z]/", '', $alphabet);
    }

}
