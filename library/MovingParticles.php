<?php

/**
 * Class MovingParticles
 * @author Andrii Tomchyshyn
 */
class MovingParticles extends AbstractModule
{

    /**
     * @param int    $speed speed
     * @param string $init  initial chamber state
     *
     * @return array
     * @throws Exception
     */
    public function animate(int $speed, string $init) : array
    {
        $this->validateInit($init);
        $this->validateSpeed($speed);

        $trace      = [];
        $decor      = [0 => '.', 1 => 'X'];
        $maskLeft   = preg_replace(["/[^L]/", "/L/"], [0, 1], $init);
        $maskRight  = preg_replace(["/[^R]/", "/R/"], [0, 1], $init);
        $chamberLen = strlen($init);

        while (true) {
            if (($chamberLen / 2) < strlen($maskRight)) {
                // bitwise OR: ('1' (ascii 49)) | ('0' (ascii 48)) = 49
                $fullMask = str_pad($maskRight, $chamberLen, 0, STR_PAD_LEFT)
                    | str_pad($maskLeft, $chamberLen, 0, STR_PAD_RIGHT);

                $state = str_replace(array_keys($decor), $decor, $fullMask);
            } else {
                $maskLeft = str_replace(array_keys($decor), $decor, $maskLeft);
                $maskRight = str_replace(array_keys($decor), $decor, $maskRight);

                $state = $maskLeft . str_repeat('.', $chamberLen - strlen($maskRight) - strlen($maskLeft)) . $maskRight;
            }

            $trace[] = $state;

            if (empty(ltrim($maskLeft, '0.')) && empty(rtrim($maskRight, '0.'))) {
                break;
            }

            $this->moveRight($maskRight, $speed);
            $this->moveLeft($maskLeft, $speed);
        }

        return $trace;
    }

    /**
     * @param string $init initial chamber state
     *
     * @return string
     * @throws Exception
     */
    public function validateInit(string $init) : string
    {
        if (false == preg_match("/^[RL\.]+$/", $init)) {
            throw new Exception(
                'Init parameter defines initial chamber state.'
                . ' It must be not empty and can contain only next characters: \'L\' \'R\' \'.\''
            );
        }

        return $init;
    }

    /**
     * @param int $speed speed
     *
     * @return int
     * @throws Exception
     */
    public function validateSpeed(int $speed) : int
    {
        if ($speed < 1) {
            throw new Exception('Speed parameter must be a positive integer value!');
        }

        return $speed;
    }

    /**
     * @param string $mask  mask
     * @param int    $speed speed
     *
     * @return void
     */
    protected function moveLeft(string &$mask, int $speed) : void
    {
        if ($speed > 0) {
            $mask = substr($mask, $speed);
        }
    }

    /**
     * @param string $mask  mask
     * @param int    $speed speed
     *
     * @return void
     */
    protected function moveRight(string &$mask, int $speed) : void
    {
        if ($speed > 0) {
            $mask = substr($mask, 0, strlen($mask) - $speed);
        }
    }

}
